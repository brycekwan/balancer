require "balancer/version"
require "balancer/investment"
require "balancer/investment_calculator"
require "balancer/investment_serializer" 
require "balancer/portfolio" 
require "balancer/portfolio_balancer" 
require "balancer/transaction_factory" 
require "balancer/transaction_presenter" 


module Balancer
  def self.rebalance(data)
    investments = InvestmentSerializer.serialize(data)
    portfolio = Portfolio.new(investments)
    portfolio_balancer = PortfolioBalancer.new(portfolio)
    transactions = portfolio_balancer.generate_transactions
    TransactionPresenter.format(transactions)
  end 
end
