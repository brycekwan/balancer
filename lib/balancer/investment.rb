require 'ostruct' 
require 'bigdecimal'

class Investment < OpenStruct
  def market_value
    BigDecimal.new(self.shares_owned, 5) * BigDecimal.new(self.share_price, 5)
  end 

  def allocated_percentage_difference
    percentage_difference = BigDecimal.new(self.actual_allocation, 5) - BigDecimal.new(self.target_allocation, 5)
    percentage_difference.abs
  end 

  def overweight? 
    self.actual_allocation > self.target_allocation
  end 

  def underweight? 
    self.actual_allocation < self.target_allocation
  end 
end 
