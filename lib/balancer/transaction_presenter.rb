class TransactionPresenter
  def self.format(transactions)
    transactions.map do |transaction|
      "#{transaction.action} #{transaction.shares} shares of #{transaction.ticker}"
    end     
  end 
end 
