class TransactionFactory
  Transaction = Struct.new(:ticker, :action, :shares, :share_price) do
    def total_value
      BigDecimal.new(share_price, 5) * BigDecimal.new(shares, 5)
    end 
  end 

  def self.build_sell_transactions(portfolio_value, investments)
    investments.map do |investment|
      shares_to_sell = InvestmentCalculator.shares_to_rebalance(portfolio_value, investment)  
      Transaction.new(investment.ticker, "sell", shares_to_sell, investment.share_price)
    end 
  end 

  def self.build_buy_transactions(portfolio_value, investments, capital)
    investments.map do |investment|
      shares_to_buy = InvestmentCalculator.shares_to_rebalance(portfolio_value, investment, capital)  
      next if shares_to_buy == 0
      transaction = Transaction.new(investment.ticker, "buy", shares_to_buy, investment.share_price)
      capital = capital - transaction.total_value
      transaction 
    end.compact
  end 
end 
