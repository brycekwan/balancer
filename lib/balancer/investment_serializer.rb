class InvestmentSerializer
  def self.serialize(data)
    return [] unless data
    data.map{|args| Investment.new(args)}
  end 
end 
