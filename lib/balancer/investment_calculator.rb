class InvestmentCalculator
  def self.shares_to_rebalance(portfolio_value, investment, maximum_capital=portfolio_value)
    capital_needed = portfolio_value * investment.allocated_percentage_difference
    capital_available = [maximum_capital, capital_needed].min
    shares = capital_available / investment.share_price
    [shares.floor, investment.shares_owned].min
  end
end 
