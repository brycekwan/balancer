class Portfolio
  def initialize(investments)
    @investments = investments
  end 

  def total_value
    return 0 unless @investments
    @investments.inject(0.0){|sum, investment| sum + investment.market_value}
  end 

  def overweight_investments
    return [] unless @investments
    @investments.select(&:overweight?)
  end 

  def underweight_investments
    return [] unless @investments
    @investments.select(&:underweight?)
  end 
end 
