class PortfolioBalancer

  def initialize(portfolio)
    @portfolio = portfolio
  end 

  def generate_transactions
    return [] if no_investments_to_buy_or_sell 

    sell_transactions = build_sell_transactions_for_overweight_investments
    buy_transactions = build_buy_transactions_for_underweight_investments(sell_transactions)

    sell_transactions.concat(buy_transactions)
  end 

  private 

  def no_investments_to_buy_or_sell
    @portfolio.underweight_investments.empty? || @portfolio.overweight_investments.empty?
  end 

  def build_sell_transactions_for_overweight_investments
    TransactionFactory.build_sell_transactions(@portfolio.total_value, @portfolio.overweight_investments)
  end 

  def build_buy_transactions_for_underweight_investments(sell_transactions)
    available_funds = available_capital_from_selling(sell_transactions)

    TransactionFactory.build_buy_transactions(@portfolio.total_value, @portfolio.underweight_investments, available_funds)
  end 

  def available_capital_from_selling(transactions)
    transactions.inject(BigDecimal.new("0")){|sum, transaction| sum + transaction.total_value}
  end 
end 
