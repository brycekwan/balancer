# Balancer

Quick tool to help balance your investment portfolio. 

## Description

This simple application will take input in the form of a hash and output to the user what share, how many shares should be sold and bought. The Balancer itself is a co-ordinator for the many steps that are taken to make the computation possible. 

## Developer Notes

A brief description of the structure is outlined below. 

**Serializer** - The InvestmentSerializer was a very naive implementation to take the user input and convert it to a collection of Invesetment objects. The serializer allows the flexibility to change and manipulate the incoming data to conform to the necessary format that the Balancer needs to do the computations. For the tests, it is assumed that the attributes from the data match what the balancer is looking for. For different forms of data sets the serializer can be used to remap the fields to work with the Balancer. 

**Portfolio** - The portfolio object acts somewhat as an aggregator for the Investments. It fits with the real life model that the investments belong to a single portfolio.

**PortfolioBalancer** - The balancer is where the balancing algorithm resides. This is where the calculation is performed and the final transactions are generated.

**TransactionFactory** - The factory is responsible for building the transactions that are generated from the investments. Depending on what needs to be done with the investment it will either be a sell or a buy transaction.

**InvestmentCalculator** - The calculator class was created to help with computations on the Investment object. See notes below for additional changes that could be made.

**Transaction** - Holds the information about each individual transaction that should be built. A lazy class was built using just a simple Struct, but certainly could be more heavy handed in a more complex algorithm.

**TransactionPresenter** - The presenter is just used to format a transaction for display to the caller in a friendly user string format. This presenter can be modified so that the Transaction object can be manipulated to display or output in any format be it XML, JSON and just plain text.

## To Dos & Enhancements

Given time constraints there were a few things that need to be cleaned up in this code. 

1. Use of BigDecimal for all computations. The initial implementation for the serializer was too naive. Since the code deals with money and numbers, it's imperative to have accuracy and precision that BigDecimal gives. Some parts of the application were corrected later on, but it the initial serialization of the data should have been done in BigDecimal which would have led to a cleaner solution for the rest of the objects. 

2. Removing logic from the TransactionFactory. A lot of the computations should really be shifted to the InvestmentCalculator rather than the Factory. The factory should really only be concerned with building transactions. A simplification can be done to only have a build transaction from raw data coming from the InvestmentCalculator.

3. The approach to rebalancing was simple and naive. It considers the amount that needs to be sold and then just uses the capital available from selling to buy shares that are under allocated. This works well for simple cases, but the better approach would be to order the investments by how far they are from their target and buy those shares first. This would give a closer spread between the actual and the target.

4. Error handling and validation. Not much validation was done given that it is assumed the data comes into the application in a particular format. The goal was to have the serializer deal with any data mapping needs. However, having validation and exceptions would definitely make the application more robust. Some things not really considered is if the allocations for the portfolio did not equal to 100% the algorithm would break down. These could have been checked and exceptions thrown for the user to handle in the outside application.

## Installation

Add this line to your application's Gemfile:

    gem 'balancer'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install balancer

## Usage

Invoke the balancer by calling the rebalance method: 

```
#!ruby

Balancer.rebalance(data)
```

The Balancer expects data to be in a format similar to the one below: 


```
#!ruby

data = [
      {ticker: "GOOG", target_allocation: 0.6, actual_allocation: 0.5096, shares_owned: 52, share_price: 98.0},
      {ticker: "AAPL", target_allocation: 0.3, actual_allocation: 0.2992, shares_owned: 136, share_price: 22.0},
      {ticker: "TSLA", target_allocation: 0.1, actual_allocation: 0.1912, shares_owned: 239, share_price: 8.0}
    ]
```


## Testing

Run either commands to run both rspec unit and integration tests:

    $ rspec spec

or

    $ rake