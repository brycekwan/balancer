require 'spec_helper' 
describe InvestmentCalculator do 
  context "#shares_to_rebalance" do 
    it "calculates shares to rebalance when overweight" do 
      investment = Investment.new(ticker: "GOOG", target_allocation: 0.6, actual_allocation: 0.5, shares_owned: 10, share_price: 25)
      shares = InvestmentCalculator.shares_to_rebalance(1000, investment)
      expect(shares).to eq(4)
    end 

    it "rounds down to the nearest full share" do 
      investment = Investment.new(ticker: "GOOG", target_allocation: 0.6, actual_allocation: 0.5, shares_owned: 10, share_price: 30)
      shares = InvestmentCalculator.shares_to_rebalance(1000, investment)
      expect(shares).to eq(3)
    end 

    it "returns shares owned when shares to balance is greater than shares owned" do 
      investment = Investment.new(ticker: "GOOG", target_allocation: 0.6, actual_allocation: 0.5, shares_owned: 10, share_price: 1)
      shares = InvestmentCalculator.shares_to_rebalance(1000, investment)
      expect(shares).to eq(10)
    end 

    it "returns shares to rebalance with limited capital" do
      investment = Investment.new(ticker: "GOOG", target_allocation: 0.6, actual_allocation: 0.5, shares_owned: 10, share_price: 10)
      shares = InvestmentCalculator.shares_to_rebalance(1000, investment, 50)
      expect(shares).to eq(5)
    end 

    it "returns 0 to rebalance with no capital" do
      investment = Investment.new(ticker: "GOOG", target_allocation: 0.6, actual_allocation: 0.5, shares_owned: 10, share_price: 10)
      shares = InvestmentCalculator.shares_to_rebalance(1000, investment, 0)
      expect(shares).to eq(0)
    end 
  end 
end 
