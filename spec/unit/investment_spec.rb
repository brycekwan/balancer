require 'spec_helper'

describe Investment do 
  context "#market_value" do 
    it "returns market value of investment" do 
      investment = Investment.new(shares_owned: 10, share_price: 10.0)
      expect(investment.market_value).to eq(100.0)
    end 
  end 

  context "#allocated_percentage_difference" do 
    it "returns absolute difference in allocation" do 
      investment = Investment.new(target_allocation: 0.6, actual_allocation: 0.5)
      expect(investment.allocated_percentage_difference).to eq(0.1)
    end 
  end 

  context "#overweight?" do
    it "returns true when over allocated" do 
      investment = Investment.new(target_allocation: 0.4, actual_allocation: 0.5)
      expect(investment.overweight?).to be(true)
    end 

    it "returns false when under allocated" do 
      investment = Investment.new(target_allocation: 0.4, actual_allocation: 0.3)
      expect(investment.overweight?).to be(false)
    end 

    it "returns false when at target allocation" do 
      investment = Investment.new(target_allocation: 0.4, actual_allocation: 0.4)
      expect(investment.overweight?).to be(false)
    end 
  end 

  context "#underweight?" do
    it "returns true when under allocated" do 
      investment = Investment.new(target_allocation: 0.4, actual_allocation: 0.3)
      expect(investment.underweight?).to be(true)
    end 

    it "returns false when over allocated" do 
      investment = Investment.new(target_allocation: 0.4, actual_allocation: 0.5)
      expect(investment.underweight?).to be(false)
    end 

    it "returns false when at target allocation" do 
      investment = Investment.new(target_allocation: 0.4, actual_allocation: 0.4)
      expect(investment.underweight?).to be(false)
    end 
    
  end 
end
