require 'spec_helper'

describe TransactionPresenter do
  it "formats many transactions" do 
    transaction_1 = double("Transaction", ticker: "GOOG", shares: 10, action: "buy")
    transaction_2 = double("Transaction", ticker: "AAPL", shares: 50, action: "sell")
    transactions = [transaction_1, transaction_2]

    expect(TransactionPresenter.format(transactions)).to eq(["buy 10 shares of GOOG", "sell 50 shares of AAPL"])
  end 
end 
