require 'spec_helper'

describe Portfolio do 
  context "#total_value" do 
    it "calculates total value of investments" do 
      portfolio = build_portfolio

      expect(portfolio.total_value).to eq(12400.0)
    end 

    it "returns 0 when no investments" do
      portfolio = Portfolio.new([])

      expect(portfolio.total_value).to eq(0)
    end 

    it "returns 0 when nil investments" do
      portfolio = Portfolio.new(nil)

      expect(portfolio.total_value).to eq(0)
    end 
  end

  context "#overweight_investments" do 
    it "returns investments that are over allocated" do
      portfolio = build_portfolio

      investments = portfolio.overweight_investments

      expect(investments.size).to eq(1)
      expect(investments[0].ticker).to eq("AAPL")
    end 

    it "returns empty array when nothing overweight" do 
      investments = [
        Investment.new({ticker: "GOOG", target_allocation: 0.6, actual_allocation: 0.5, shares_owned: 10, share_price: 10.0}),
      ]
      portfolio = Portfolio.new(investments)

      investments = portfolio.overweight_investments

      expect(investments).to be_empty
    end 
    
    it "returns empty array when nil investments" do 
      portfolio = Portfolio.new(nil)

      investments = portfolio.overweight_investments

      expect(investments).to eq([])
    end 
  end 

  context "#underweight_investments" do 
    it "returns investments that are under allocated" do
      portfolio = build_portfolio

      investments = portfolio.underweight_investments

      expect(investments.size).to eq(1)
      expect(investments[0].ticker).to eq("GOOG")
    end 

    it "returns empty array when nothing underweight" do 
      investments = [
        Investment.new({ticker: "GOOG", target_allocation: 0.4, actual_allocation: 0.5, shares_owned: 10, share_price: 10.0}),
      ]
      portfolio = Portfolio.new(investments)

      investments = portfolio.underweight_investments

      expect(investments).to be_empty
    end 
    
    it "returns empty array when nil investments" do 
      portfolio = Portfolio.new(nil)

      investments = portfolio.underweight_investments

      expect(investments).to eq([])
    end 
  end 

  def build_portfolio
    investments = [
      Investment.new({ticker: "GOOG", target_allocation: 0.6, actual_allocation: 0.5, shares_owned: 10, share_price: 10.0}),
      Investment.new({ticker: "AAPL", target_allocation: 0.2, actual_allocation: 0.3, shares_owned: 100, share_price: 123.0})
    ]
    
    Portfolio.new(investments)
  end 
end 
