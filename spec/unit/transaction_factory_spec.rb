require 'spec_helper'

describe TransactionFactory do 
  context "#build_sell_transactions" do 
    it "generates sell transactions for investments" do 
      overweight_investments = [Investment.new(ticker: "AAPL", target_allocation: 0.4, actual_allocation: 0.5, shares_owned: 20, share_price: 25.0)]

      transactions = TransactionFactory.build_sell_transactions(1000, overweight_investments)

      expect(transactions.size).to eq(1)
      expect(transactions[0].ticker).to eq("AAPL")
      expect(transactions[0].action).to eq("sell")
      expect(transactions[0].shares).to eq(4)
    end 
  end 

  context "#build_buy_transactions" do 
    it "generates buy transactions for investments" do 
      investments = [Investment.new(ticker: "GOOG", target_allocation: 0.6, actual_allocation: 0.5, shares_owned: 20, share_price: 25.0)]

      transactions = TransactionFactory.build_buy_transactions(1000, investments, 1000)

      expect(transactions.size).to eq(1)
      expect(transactions[0].ticker).to eq("GOOG")
      expect(transactions[0].action).to eq("buy")
      expect(transactions[0].shares).to eq(4)
    end 

    it "generates buy transactions for investments with no capital for second investment" do 
      investments = [
        Investment.new(ticker: "AAPL", target_allocation: 0.6, actual_allocation: 0.5, shares_owned: 20, share_price: 25.0),
        Investment.new(ticker: "GOOG", target_allocation: 0.4, actual_allocation: 0.3, shares_owned: 20, share_price: 30.0)
      ]

      transactions = TransactionFactory.build_buy_transactions(1000, investments, 100)

      expect(transactions.size).to eq(1)
      expect(transactions[0].ticker).to eq("AAPL")
      expect(transactions[0].action).to eq("buy")
      expect(transactions[0].shares).to eq(4)
    end 

    it "generates buy transactions for investments with not enough capital for second investment" do 
      investments = [
        Investment.new(ticker: "AAPL", target_allocation: 0.6, actual_allocation: 0.5, shares_owned: 20, share_price: 25.0),
        Investment.new(ticker: "GOOG", target_allocation: 0.4, actual_allocation: 0.3, shares_owned: 20, share_price: 30.0)
      ]

      transactions = TransactionFactory.build_buy_transactions(1000, investments, 150)

      expect(transactions.size).to eq(2)
      expect(transactions[0].ticker).to eq("AAPL")
      expect(transactions[0].shares).to eq(4)

      expect(transactions[1].ticker).to eq("GOOG")
      expect(transactions[1].shares).to eq(1)
    end 
  end 
end 
