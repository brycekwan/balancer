require 'spec_helper'

describe PortfolioBalancer do 
  context "#generate_transactions" do 
    it "generates no transactions when portfolio has no overweight investments" do 
      portfolio = double(overweight_investments: [], underweight_investments: [{}])
      balancer = PortfolioBalancer.new(portfolio)

      transactions = balancer.generate_transactions

      expect(transactions).to be_empty
    end 

    it "generates no transactions when portfolio has no underweight investments" do 
      portfolio = double(overweight_investments: [{}], underweight_investments: [])
      balancer = PortfolioBalancer.new(portfolio)

      transactions = balancer.generate_transactions

      expect(transactions).to be_empty
    end 

    it "returns sell and buy transactions" do 
      portfolio = double(overweight_investments: [{}], underweight_investments: [{}], total_value: 1000)
      expect(TransactionFactory).to receive(:build_sell_transactions) {[double(total_value: 100)]}
      expect(TransactionFactory).to receive(:build_buy_transactions) {[double]}

      balancer = PortfolioBalancer.new(portfolio)

      transactions = balancer.generate_transactions

      expect(transactions.size).to eq(2)
    end 
  end 
end 
