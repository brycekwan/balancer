require 'spec_helper'
describe InvestmentSerializer do 
  context "#serialize" do 
    it "serializes data" do 
      data = [
        {ticker: "GOOG", target_allocation: 0.6, actual_allocation: 0.5, shares_owned: 10, share_price: 10.0},
        {ticker: "AAPL", target_allocation: 0.2, actual_allocation: 0.3, shares_owned: 100, share_price: 123.0}
      ]

      investments = InvestmentSerializer.serialize(data)
      
      expect(investments.size).to eq(2)
      expect(investments[0].ticker).to eq("GOOG")
      expect(investments[1].ticker).to eq("AAPL")
    end 

    it "returns empty set when no data" do 
      investments = InvestmentSerializer.serialize([])

      expect(investments).to eq([])
    end 

    it "returns empty set when nil data" do 
      investments = InvestmentSerializer.serialize(nil)

      expect(investments).to eq([])
    end 
  end 
end 
