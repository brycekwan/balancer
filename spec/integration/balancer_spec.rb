require 'spec_helper'
describe Balancer do
  it "rebalances shares for a person's portfolio" do 
    data = [
      {ticker: "GOOG", target_allocation: 0.6, actual_allocation: 0.5096, shares_owned: 52, share_price: 98.0},
      {ticker: "AAPL", target_allocation: 0.3, actual_allocation: 0.2992, shares_owned: 136, share_price: 22.0},
      {ticker: "TSLA", target_allocation: 0.1, actual_allocation: 0.1912, shares_owned: 239, share_price: 8.0}
    ]

    output = Balancer.rebalance(data)
    expect(output).to include("buy 9 shares of GOOG")
    expect(output).to include("sell 114 shares of TSLA")
  end 
end 
